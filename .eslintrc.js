module.exports = {
  root: false,
  
  env: {
    node: true
  },
  extends: ["plugin:vue/essential"],
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "max-len": ["error", { "code": 360 }]
  },
  // rules: {
  //   'prettier/prettier': 0,
  // },
  parserOptions: {
    parser: "babel-eslint"
  }
};
