// action types
export const APPEND_BREADCRUM = "appendBreadcrumb";

// mutation types
export const SET_DATA = "setBreadcrumb";
export const ADD_DATA = "addBreadcrumb";

export default {
    state: {
        countReport: []
    },
    getters: {
        /**
         * Get list of breadcrumbs for current page
         * @param state
         * @returns {*}
         */
        countReport(state) {
            return state.countReport;
        }
    },
    actions: {
        /**
         * Set the breadcrumbs list
         * @param state
         * @param payload
         */
        [SET_DATA](state, payload) {
            state.commit(SET_DATA, payload);
        },

        /**
         * Add breadcrumb
         * @param state
         * @param payload
         */
        [ADD_DATA](state, payload) {
            if (typeof payload === "object") {
                payload.forEach(item => state.commit(APPEND_BREADCRUM, item));
            } else {
                state.commit(APPEND_BREADCRUM, payload);
            }
        }
    },
    mutations: {
        [APPEND_BREADCRUM](state, payload) {
            state.breadcrumbs = [...state.breadcrumbs, payload];
        },
        [SET_DATA](state, payload) {
            state.breadcrumbs = payload;
        }
    }
};
