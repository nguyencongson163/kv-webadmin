import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  routes: [{
      path: "/",
      redirect: "/trang-chu/",
      component: () => import("@/view/layout/Layout"),
      children: [{
          path: "/trang-chu",
          name: "trang-chu",
          component: () => import("@/view/pages/category/Category.vue"),
          children: [{
            path: "/",
            component: () =>
              import("@/view/pages/category/ParentCategory.vue")
          }]
        },
        {
          path: "/danh-muc-con",
          component: () => import("@/view/pages/category/Category.vue"),
          children: [{
            path: "/",
            component: () => import("@/view/pages/category/SubCategory.vue")
          }]
        },
        {
          path: "/san-pham",
          component: () => import("@/view/pages/product/Product.vue"),
          children: [{
            path: "/",
            component: () => import("@/view/pages/product/ListData.vue")
          }]
        },
        {
          path: "/khu-vuc",
          component: () => import("@/view/pages/area/Area.vue"),
          children: [{
            path: "/",
            component: () => import("@/view/pages/area/ListArea.vue")
          }]
        },
        {
          path: "/tai-khoan",
          component: () => import("@/view/pages/account/Account.vue"),
          children: [{
            path: "/",
            component: () => import("@/view/pages/account/ListAccount.vue")
          }]
        },
        {
          path: "/khuyen-mai",
          component: () => import("@/view/pages/promotion/Promotion.vue"),
          children: [{
            path: "/",
            component: () =>
              import("@/view/pages/promotion/ListPromotion.vue")
          }]
        },
        {
          path: "/bai-viet",
          component: () => import("@/view/pages/blog/Blog.vue"),
          children: [{
              path: "/",
              component: () =>
                import("@/view/pages/blog/List.vue")
            },
            {
              path: "tao-bai-viet",
              name: 'tao-bai-viet',
              component: () =>
                import("@/view/pages/blog/Create.vue")
            },
            {
              path: "sua-bai-viet/:id",
              name: 'sua-bai-viet/',
              component: () =>
                import("@/view/pages/blog/Update.vue"),
              props: true
            },
          ]
        },
        {
          path: "/the-tag",
          component: () => import("@/view/pages/tag/Tag.vue"),
          children: [{
            path: "/",
            component: () =>
              import("@/view/pages/tag/List.vue")
          }, ]
        },
        {
          path: "/dat-lich",
          component: () => import("@/view/pages/apointment/Appointment.vue"),
          children: [{
            path: "/",
            component: () =>
              import("@/view/pages/apointment/List.vue")
          }, ]
        },
        {
          path: "/cai-dat",
          component: () => import("@/view/pages/setting/Setting.vue"),
          children: [{
            path: "/",
            component: () =>
              import("@/view/pages/setting/Update.vue")
          }, ]
        }
      ]
    },
    {
      path: "/",
      component: () => import("@/view/pages/auth/Auth"),
      children: [{
        name: "login",
        path: "/login",
        component: () => import("@/view/pages/auth/Login")
      }]
    },
    {
      path: "*",
      redirect: "/404"
    },
    {
      // the 404 route, when none of the above matches
      path: "/404",
      name: "404",
      component: () => import("@/view/pages/error/Error-1.vue")
    }
  ]
});