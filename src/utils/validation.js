exports.isValidPhoneNumber = value => {
  if (typeof value === 'undefined' || value === null || value === '') {
    return true;
  }
  return /(03|07|08|09|01[2|6|8|9])+([0-9]{8})\b/.test(value);
};
